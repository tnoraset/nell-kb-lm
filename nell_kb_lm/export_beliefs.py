#!/usr/bin/env python

from kb import belief
from kb import models
def main(belief_file='../data/nell/NELL.08m.790.esv.csv.gz', output_file="../data/belief.pick", type="pick"):
	print "reading belief file:"
	print belief_file 
	entities = belief.parse_nell_belief_file(belief_file)
	print "done reading"
	print len(entities)
	
	if type == 'json':
		f = open (output_file, "w")
		#TODO: json doesn't work with namedtuple
		import json
		json.dump(entities, f)
		f.close()
	elif type == 'pickle':
		f = open (output_file, "w")
		import cPickle as pickle
		pickle.dump(entities, f)
		f.close()
	else :
		import pymongo
		client = pymongo.MongoClient('downey-n2.cs.northwestern.edu', 27017)
		db = client.nell
		db.authenticate('pgm','EECS_395_495_NELL')
		entity_collection = db.entity_expanded
		entity_collection.drop()
		for key in entities :
			entity = entities[key]
			entity_collection.insert(models.entity2dict(entity))
		print 'indexing'
		entity_collection.ensure_index([("name", pymongo.ASCENDING)])
		entity_collection.ensure_index([("short_name", pymongo.ASCENDING)])
		entity_collection.ensure_index([("surfaces", pymongo.ASCENDING)])
		entity_collection.ensure_index([("categories.val_name", pymongo.ASCENDING)])
		client.close()
	print "done writing"
	return entities

import sys
if __name__ == '__main__':
	entities = None
	if len(sys.argv) < 4:
		entities = main()
	else :
		entities = main(sys.argv[1], sys.argv[2], sys.argv[3])
