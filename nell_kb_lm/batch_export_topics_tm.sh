for file in ../data/topicmodels/op-a0.5-i200-k150.topics
do

out=`echo $file | awk -F"/" '{ print $4 }'`
echo $out
python export_topics_r_lda.py ../data/testset/all.map.json $file ../data/testset/all.strings ../data/testset/tm_topics/"$out" >> log

done
