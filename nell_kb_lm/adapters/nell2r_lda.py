#!/usr/bin/env python
import pymongo
import math
import json


def __get_concept_list__():
	client = pymongo.MongoClient('downey-n2.cs.northwestern.edu', 27017)
	db = client.nell
	db.authenticate('pgm','EECS_395_495_NELL')
	entity_collection = db.entity_expanded
	concepts = entity_collection.find()
	entity_list=[]
	i = 0
	for concept in concepts:
		entity_list.append(str(concept[u'name']))
		i=i+1
		#if i > 10 : break
		if i % 100000 == 0 : print i
	client.close()
	return entity_list

def __get_concepts_from_file__(file_name) :
	file = open(file_name, 'r')
	entity_list = []
	for line in file :
		entity_list.append(line.strip())
	file.close()
	return entity_list

def write_vocab(out_filename, concept_list):
	f = open(out_filename, 'w')
	for l in concept_list:
		if unicode(l) in concept_set:
			f.write(l + '\n')
	f.close()

def __get__prob__(relations, val_name, rel_name='generalizations') :
	for relation in relations :
		if relation[u'rel_name'] == rel_name and relation[u'val_name'] == val_name :
			prob = relation[u'prob']
			if math.isnan(prob): return 1.0
			return prob
	return 0

def __get_concepts__(entity_collection, val_name, rel_name='generalizations', concept_dict=None):
	field = u'categories'
	query = {u'categories.val_name':unicode(val_name)}
	if rel_name != 'generalizations' :
		query = {u'relations.val_name':unicode(val_name), u'relations.re_name':unicode(rel_name)}
		field = u'relations'
	concepts = entity_collection.find(query)
	out_concepts = []
	for concept in concepts:
		name = concept[u'name']
		prob = __get__prob__(concept[field], val_name, rel_name)
		if concept_dict != None :
			if not str(name) in concept_dict : continue
			name = concept_dict[str(name)]
		out_concepts.append((name, int(prob*10)))
	return out_concepts

def __read_concept_set__(filename):
	f = open(filename)
        str_map =  json.load(f)
        f.close()
        concept_set = set()
	for key in str_map :
		concepts = str_map[key]
		#print concepts
		concept_set = concept_set.union(concepts)
	return concept_set

def __convert_list_to_dict__(concept_list) :
	concept_dict = {}
	for i in range(len(concept_list)):
		concept_dict[concept_list[i]] = i
	return concept_dict

def write_cat_docs(cat_name_list, entity_collection, concept_list, output_path='') :
	for cat in cat_name_list :
		cat = cat.strip()
		concept_dict = __convert_list_to_dict__(concept_list)
		concepts = __get_concepts__( entity_collection = entity_collection, val_name = cat, concept_dict=concept_dict)
		print cat + ': ' + str(len(concepts)) + ' concepts'
		f = open(output_path+str(cat+'.csv'), 'w')
		print 'saving concepts to ' + output_path+str(cat+'.csv')
		for concept in concepts :
			#if concept[0] in concept_set:
			f.write(str(concept[0]) +','+ str(concept[1]) + '\n')
			#for i in range(concept[1]):
			#	f.write(str(concept[0]) +','+ str(1) + '\n')
		f.close()

import sys
if __name__ == '__main__':
	global concept_set
	concept_set = __read_concept_set__('../../data/tmpset/all.map.json')
	print len(concept_set)
	if len(sys.argv) < 2 :
		print 'need 2 args: cat-file and outpath'
	else :
		f = open(sys.argv[1], 'r')
		outpath = '../../data/lda_data/'
		if len(sys.argv)  > 2 :
			outpath = sys.argv[2]
		print 'reading concepts'
		concept_list = []
		if len(sys.argv) > 3 :
			concept_list = __get_concepts_from_file__(sys.argv[3])
		else :
			concept_list = __get_concept_list__()
		print len(concept_list)
		print 'writing vocab'
		write_vocab(out_filename = outpath + 'vocab', concept_list = concept_list)
		client = pymongo.MongoClient('downey-n2.cs.northwestern.edu', 27017)
		db = client.nell
		db.authenticate('pgm','EECS_395_495_NELL')
		entity_collection = db.entity
		print 'write documents'
		write_cat_docs(cat_name_list=f, entity_collection = entity_collection, concept_list=concept_list, output_path=outpath)
		print 'done'
		f.close()
		client.close()

