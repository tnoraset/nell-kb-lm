#!/usr/bin/env python

'''
Convert NELL KB into dataset for PGM class's homework 4
'''



def __add_unique_id__(dictionary, key) :
	# if not key.startswith('concept:'):
	# 	key = 'concept:' + key
	if key not in dictionary :
		dictionary[key] = len(dictionary)

def build_dictionaries(entities) :
	ent_dict={}
	rel_dict={}
	for entity_key in entities :
		entity = entities[entity_key]
		__add_unique_id__(ent_dict, entity.name)
		for relation in entity.relations :
			__add_unique_id__(rel_dict, str(relation.rel_name+"-"+relation.val_name))
		for relation in entity.categories :
			__add_unique_id__(rel_dict, str(relation.rel_name+"-"+relation.val_name))
	return (ent_dict, rel_dict)

def build_dataset(entities, ent_dict, rel_dict):
	dataset = []
	for entity_key in entities :
		entity = entities[entity_key]
		if entity_key not in ent_dict :
			print(str(entity_key + " is not found in the dictionary"))
			continue
		entity_id = ent_dict[entity_key]
		for relation in entity.relations :
			relation_key = str(relation.rel_name+"-"+relation.val_name)
			if relation_key not in rel_dict :
				print(str(relation_key + "is not found in the dictionary"))
				continue
			relation_id = rel_dict[relation_key]
			dataset.append((entity_id, relation_id))
		for relation in entity.categories :
			relation_key = str(relation.rel_name+"-"+relation.val_name)
			if relation_key not in rel_dict :
				print(str(relation_key + "is not found in the dictionary"))
				continue
			relation_id = rel_dict[relation_key]
			dataset.append((entity_id, relation_id))
	return dataset

def write_dataset(file, dataset):
	for point in dataset :
		file.write(str(point[0]) + " " + str(point[1]) + "\n")
	file.flush()

def write_dictionary(file, dictionary):
	for key in dictionary :
		file.write(key + "\t" + str(dictionary[key]) + "\n")
	file.flush()

if __name__ == '__main__':
	a = {}
	__add_unique_id__(a,"a")
	__add_unique_id__(a,"b")
	__add_unique_id__(a,"b")
	print a