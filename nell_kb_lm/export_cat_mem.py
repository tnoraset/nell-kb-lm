#!/usr/bin/env python


import json
import pymongo
def load_map(filename):
	f = open(filename)
	str_map =  json.load(f)
	f.close()
	return str_map

def get_cat_dict(catname_list):
	cat_dict = {}
	i = 0
	for cat in catname_list:
		cat = cat.strip()
		cat_dict[cat] = i
		i = i + 1

	return cat_dict

def get_cat_memlist(cat_dict, concept_name, entity_collection) :
	memlist = [0 for i in range(275)]
	entity = entity_collection.find_one({u'name':concept_name})
	categories = entity[u'categories']
	for category in categories :
		cname = category[u'val_name']
		idx = cat_dict[cname]
		memlist[idx] = 1
	return memlist

def union_memlist(list1, list2):
	out_list = list(list1)
	for i in range(len(list2)) : 
		if list2[i] == 1: out_list[i] = 1
	return out_list

if __name__ == '__main__':
	cf = open('../data/category-name.txt','r')
	cat_dict = get_cat_dict(cf)
	cf.close()
	sm = load_map('../data/testset/all.map.json')
	client = pymongo.MongoClient('downey-n2.cs.northwestern.edu', 27017)
	db = client.nell
	db.authenticate('pgm','EECS_395_495_NELL')
	entity_collection = db.entity_expanded
	j = 0
	fout = open('../data/testset/cat_mem.topics', 'w')	
	for key in sm:
		key_memlist = [0 for i in range(275)]
		for concept in sm[key]:
			c_memlist = get_cat_memlist(cat_dict, concept, entity_collection)
			key_memlist = union_memlist(key_memlist, c_memlist)
		fout.write(key)
		print len(key_memlist)
		for val in key_memlist :
			fout.write('\t'+str(val))
		fout.write('\n')		
	fout.close()
	client.close()

