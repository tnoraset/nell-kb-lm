#!/usr/bin/env python

import json

def load_map(filename):
	f = open(filename)
	str_map =  json.load(f)
	f.close()
	return str_map

def __get_prob__(str) :
	if str == 'NA' :
		return 1/float(100)
	else:
		return float(str)

def load_topics(filename):
	f = open(filename)
	topic_map = {}
	f.readline()
	for line in f :
		parts = line.split(' ')
		name = parts.pop(0).strip(' "')
		vector = [__get_prob__(x.strip()) for x in parts ]
		topic_map[name] = vector
	f.close()
	return topic_map

def average_topics(vector_list) :
	avg_vector = [0 for i in range(100)]
	for vector in vector_list:
		for i in range(len(vector)) :
			avg_vector[i] = avg_vector[i] + vector[i]
	count = len(vector_list)
	for i in range(100) :
		avg_vector[i] = avg_vector[i] / float(count)
	return avg_vector

def get_default_vector():
	return [1/float(100) for i in range(100)]

def get_topics(str_map, topic_map, str_name) :
	if unicode(str_name) not in str_map :
		print 'unrecognized string: ' + str_name
		return get_default_vector()
	concepts = str_map[unicode(str_name)]
	if len(concepts) == 0 :
		print 'no concept found for ' + str_name
		return get_default_vector()
	vector_list = []
	for concept in concepts :
		concept = str(concept)
		if concept not in topic_map :
			print concept + ' concept is not in topic'
			continue
		vector_list.append(topic_map[concept])
	if len(vector_list) == 0: return get_default_vector()
	return average_topics(vector_list)

if __name__ == '__main__':
	import sys
	str_map_file = sys.argv[1]
	topic_map_file = sys.argv[2]
	input_file = sys.argv[3]
	output_file = sys.argv[4]
	print 'loading string map'
	str_map = load_map(str_map_file)
	print 'loading topic map'
	topic_map = load_topics(topic_map_file)
	f_in = open(input_file)
	f_out = open(output_file, 'w')
	for line in f_in :
		if line.startswith('#') : continue
		line = line.strip()
		vector = get_topics( str_map, topic_map, line)
		f_out.write(line)
		for v in vector :
			f_out.write('\t'+str(v))
		f_out.write('\n')
	f_in.close()
	f_out.close()
