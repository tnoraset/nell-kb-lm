#!/usr/bin/env python

import cPickle as pickle
from adapters import wzc

f = open('../data/belief.pick')
a = pickle.load(f)
f.close()
(ed, rd) = wzc.build_dictionaries(a)
dataset = wzc.build_dataset(a, ed, rd)

f = open('../data/data.txt', 'w')
wzc.write_dataset(f, dataset)
f.close()

f = open('../data/entity_dictionary.txt', 'w')
wzc.write_dictionary(f, ed)
f.close()


f = open('../data/rel_val_dictionary.txt', 'w')
wzc.write_dictionary(f, rd)
f.close()