i=$1

python export_topics_r_lda.py ../data/testset/all.map.json ../data/lda_data$i/a.1.eta.1.ontology.normalized.topics ../data/testset/all.strings ../data/testset/$i.a.1.etai.1.ontology.normalized.topics

python export_topics_r_lda.py ../data/testset/all.map.json ../data/lda_data$i/a1.eta1.ontology.normalized.topics ../data/testset/all.strings ../data/testset/$i.a1.eta1.ontology.normalized.topics

python export_topics_r_lda.py ../data/testset/all.map.json ../data/lda_data$i/a10.eta10.ontology.normalized.topics ../data/testset/all.strings ../data/testset/$i.a10.eta10.ontology.normalized.topics

python export_topics_r_lda.py ../data/testset/all.map.json ../data/lda_data$i/a100.eta100.ontology.normalized.topics ../data/testset/all.strings ../data/testset/$i.a100.eta100.ontology.normalized.topics
