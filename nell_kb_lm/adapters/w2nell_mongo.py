#!/usr/bin/env python

'''
Convert test set into NELL concept name
'''
	
def __concept_names__(concepts) :
	names = []
	for entity in concepts:
		names.append(entity[u'name'])
	return names

def __clean_word__(word):
	return word.strip().replace("+", " ")

def __short_name__(word):
	return word.strip().replace("+", "_").lower()

def word2concepts(word, short_name, entity_collection) :
	concepts = []
	entities = entity_collection.find({'$or':[{u'surfaces':unicode(word)}, {u'short_name':unicode(short_name)}]})
	for entity in entities :
		concepts.append(entity)
	return concepts

def file2concepts(in_file, entity_collection) :
	concept_map = {}
	for line in in_file :
		if line.startswith('#') : continue
		word = __clean_word__(line)
		short_name = __short_name__(line)
		concepts = word2concepts(word, short_name, entity_collection)
		concept_map[line.strip()] = concepts
		return concept_map

def file2concept_names(in_file, entity_collection) :
	concept_map = {}
	for line in in_file :
		if line.startswith('#') : continue
		word = __clean_word__(line)
		short_name = __short_name__(line)
		concepts = word2concepts(word, short_name, entity_collection)
		concept_map[line.strip()] = __concept_names__(concepts)
	return concept_map

if __name__ == '__main__':
	import glob
	import os
	import pymongo
	import json
	client = pymongo.MongoClient('downey-n2.cs.northwestern.edu', 27017)
	db = client.nell
	db.authenticate('pgm','EECS_395_495_NELL')
	entity_collection = db.entity
	directory = "../data/tmpset/"
	os.chdir(directory)
	all_map = {}
	for file_name in glob.glob("*.txt") :
		out_filename = file_name + '.map.json'
		in_file = open(file_name)
		print 'processing ' + file_name
		c_map = file2concept_names(in_file, entity_collection)
		all_map.update(c_map)
		print 'done ' + str(len(c_map))
		out_file = open(out_filename, 'w')
		print 'writing ' + out_file.name
		output= json.dumps(c_map, indent=4)
		out_file.write(output)
		out_file.flush()
		out_file.close()
		in_file.close()
	client.close()
	out_filename = 'all.map.json'
	out_file = open(out_filename, 'w')
	output= json.dumps(all_map, indent=4)
	out_file.write(output)
	out_file.flush()
	out_file.close()
	out_filename = 'all.strings'
	out_file = open(out_filename, 'w')
	for key in all_map :
		out_file.write(key+'\n')
	out_file.close()
