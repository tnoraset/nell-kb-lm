from collections import namedtuple

class Entity (namedtuple("Entity", "name short_name surfaces categories relations")):
	'''
	define Entity type
	name surfaces categories relations
		name -> Entity
		surfaces -> literalStrings (with best literal string at first element)
		categories, relations -> see Relation type
	'''
	def __new__(cls, name, short_name, surfaces, categories, relations):
		return super(Entity, cls).__new__(cls, name, short_name, surfaces, categories, relations)

class Relation (namedtuple("Relation", "rel_name val_name prob iteration source candidate_sources")):
	'''
	define Relation as in a belief in NELL
	rel_name val_name prob iteration source candidate_sources
	'''
	def __new__(cls, rel_name, val_name, prob, iteration, source, candidate_sources):
		return super(Relation, cls).__new__(cls, rel_name, val_name, prob, iteration, source, candidate_sources)

def relation2dict(relation):
	return {
		"rel_name": relation.rel_name,
		"val_name": relation.val_name,
		"prob": relation.prob,
		"iteration": relation.iteration,
		"source": relation.source,
		"candidate_sources": relation.candidate_sources
	}

def entity2dict(entity) :
	relation_list = []
	category_list = []
	for relation in entity.relations :
		relation_list.append(relation2dict(relation))
	for relation in entity.categories :
		category_list.append(relation2dict(relation))
	return {
		"name": entity.name,
		"short_name": entity.short_name,
		"surfaces": entity.surfaces,
		"categories": category_list,
		"relations": relation_list
	}

def dict2relation(dict) :
	return Relation(
		rel_name=str(dict[u'rel_name']), 
		val_name=str(dict[u'val_name']), 
		prob=dict[u'prob'], 
		iteration=dict[u'iteration'], 
		source=str(dict[u'source']), 
		candidate_sources=str(dict[u'candidate_sources']))

def dict2entity(dict):
	category_list = []
	relation_list = []
	for relation in dict[u'relations'] :
		relation_list.append(dict2relation(relation))
	for relation in dict[u'categories'] :
		category_list.append(dict2relation(relation))
	return Entity(
		name=str(dict[u'name']), 
		short_name=str(dict[u'short_name']),
		surfaces=[str(surface) 
			for surface in dict[u'surfaces']], 
		categories=category_list, 
		relations=relation_list)